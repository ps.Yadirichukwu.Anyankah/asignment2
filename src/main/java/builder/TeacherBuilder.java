package builder;

import entity.Department;
import entity.Teacher;

import java.time.LocalDate;

public class TeacherBuilder implements EmployeeBuilder{

    private final Teacher teacher;

    private String subject;




    public TeacherBuilder() {
        this.teacher = new Teacher();
    }

    @Override
    public void setName(String name) {
        teacher.setName(name);
    }

    @Override
    public void setSocialSecurityNumber(Long socialSecurityNumber) {
        teacher.setSocialSecurityNumber(socialSecurityNumber);
    }

    @Override
    public void setDateOfBirth(LocalDate dateOfBirth) {
        teacher.setDateOfBirth(dateOfBirth);
    }

    @Override
    public void setDepartment(Department department) {
        teacher.setDepartment(department);
    }

    @Override
    public void setYearsOfExperience(int yearsOfExperience) {
        teacher.setYearsOfExperience(yearsOfExperience);
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    @Override
    public Teacher build() {
        return (Teacher) teacher;
    }



}
