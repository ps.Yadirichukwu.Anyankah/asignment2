package builder;

import entity.Department;
import entity.HeadOfDepartment;

import java.time.LocalDate;

public class HeadOfDeptBuilder implements EmployeeBuilder{


    private final HeadOfDepartment headOfDepartment;

    public HeadOfDeptBuilder() {
        this.headOfDepartment = new HeadOfDepartment();
    }


    @Override
    public void setName(String name) {
         headOfDepartment.setName(name);
    }

    @Override
    public void setSocialSecurityNumber(Long socialSecurityNumber) {
        headOfDepartment.setSocialSecurityNumber(socialSecurityNumber);
    }

    @Override
    public void setDateOfBirth(LocalDate dateOfBirth) {
        headOfDepartment.setDateOfBirth(dateOfBirth);
    }

    @Override
    public void setDepartment(Department department) {
        headOfDepartment.setDepartment(department);
    }

    @Override
    public void setYearsOfExperience(int yearsOfExperience) {
        headOfDepartment.setYearsOfExperience(yearsOfExperience);
    }

    @Override
    public HeadOfDepartment build() {
        return (HeadOfDepartment) headOfDepartment;
    }
}
