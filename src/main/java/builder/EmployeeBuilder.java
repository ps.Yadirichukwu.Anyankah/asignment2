package builder;

import entity.Department;
import entity.Employee;

import java.time.LocalDate;

public interface EmployeeBuilder {
    void setName(String name);
    void  setSocialSecurityNumber(Long socialSecurityNumber);
    void  setDateOfBirth(LocalDate dateOfBirth);
    void  setDepartment(Department department);
    void setYearsOfExperience(int yearsOfExperience);
    Employee build();

}
