package factory;

import entity.Department;
import builder.HeadOfDeptBuilder;
import builder.TeacherBuilder;
import entity.HeadOfDepartment;
import entity.Teacher;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BuildFactory {

    public static Teacher buildTeacher(EmployeeFactory factory, String name, Long socialSecurityNumber, int yearsOfExperience,
                                       Department department, LocalDate dateOfBirth, String subject) {
        TeacherBuilder teacherBuilder = (TeacherBuilder) factory.makeEmployeeBuilder("Teacher");
        teacherBuilder.setDateOfBirth(dateOfBirth);
        teacherBuilder.setDepartment(department);
        teacherBuilder.setYearsOfExperience(yearsOfExperience);
        if (teacherExists(socialSecurityNumber)) {
            System.out.println("User already exists");
            return null;
        }
        teacherBuilder.setSocialSecurityNumber(socialSecurityNumber);
        teacherBuilder.setSubject(subject);
        teacherBuilder.setName(name);
        return teacherBuilder.build();
    }

    private static boolean teacherExists(Long socialSecurityNumber) {
        List<Teacher> allTeachers = new ArrayList<>();
        // Assuming there is a list of all teachers somewhere called "allTeachers"
        for (Teacher teacher : allTeachers) {
            if (teacher.getSocialSecurityNumber().equals(socialSecurityNumber)) {
                return true;
            }
        }
        return false;
    }

    public static HeadOfDepartment buildHOD(EmployeeFactory eFactory, String name, Long socialSecurityNumber, int experience,
                                            Department department, LocalDate localDate) {
        HeadOfDeptBuilder headOfDeptBuilder = (HeadOfDeptBuilder) eFactory.makeEmployeeBuilder("HOD");
        headOfDeptBuilder.setDateOfBirth(localDate);
        headOfDeptBuilder.setDepartment(department);
        headOfDeptBuilder.setName(name);
        if (teacherExists(socialSecurityNumber)) {
            System.out.println("User already exists");
            return null;
        }
        headOfDeptBuilder.setSocialSecurityNumber(socialSecurityNumber);
        headOfDeptBuilder.setYearsOfExperience(experience);
        return (HeadOfDepartment) headOfDeptBuilder.build();
    }
}
