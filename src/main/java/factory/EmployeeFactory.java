package factory;

import builder.EmployeeBuilder;
import builder.HeadOfDeptBuilder;
import builder.TeacherBuilder;

public class EmployeeFactory {

        public EmployeeBuilder makeEmployeeBuilder(String employeeType) {
            EmployeeBuilder employee = null;
            if (employeeType.equalsIgnoreCase("HOD")) {
                employee = new HeadOfDeptBuilder();
            } else if (employeeType.equalsIgnoreCase("Teacher")) {
                employee = new TeacherBuilder();
            }
            return employee;
        }
    }
