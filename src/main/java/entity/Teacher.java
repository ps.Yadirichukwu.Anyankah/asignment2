package entity;

import java.time.LocalDate;

public class Teacher extends Employee {

        private String subject;


        public Teacher() {
        }
        public Teacher(String name, Long socialSecurityNumber, LocalDate dateOfBirth, Department department, String subject, int yearsOfExperience) {
            super(name, socialSecurityNumber, dateOfBirth, department, yearsOfExperience);
            this.subject = subject;
        }


    public String getSubject() {
            return subject;
        }

    public void setSubject(String subject) {
        this.subject = subject;
    }


}

