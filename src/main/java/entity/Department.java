package entity;


public enum Department {
    LANGUAGES,
    SCIENCE_MATH,
    SOCIAL_STUDIES;

    private String department;


    Department(String department) {
        department = department;
    }

    Department() {

    }

    public String getDepartment() {
        return department;
    }

}
