package entity;

import strategy.EmployeeSortStrategy;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class HeadOfDepartment extends Employee {


    private List<Teacher> departmentTeachers;

    public HeadOfDepartment(String name, Long ssn, LocalDate dateOfBirth, Department department,
                            int yearsOfExperience, List<Teacher> departmentTeachers) {
        super(name, ssn, dateOfBirth, department, yearsOfExperience);
        this.departmentTeachers = new ArrayList<>();
    }

    public HeadOfDepartment() {
        departmentTeachers = new ArrayList<>();
    }

    public List<Teacher> getDepartmentTeachers() {
        return departmentTeachers;
    }

    public void addEmployee(Teacher teacher) throws IllegalArgumentException {
        if (!getDepartment().equals(teacher.getDepartment())) {
            throw new IllegalArgumentException(
                    String.format("Teacher %s must be in %s department",
                            teacher.getName(), getDepartment().getDepartment())
            );
        }
        departmentTeachers.add(teacher);
    }

    public void displayDetails() {
        System.out.println("DEPARTMENT HEAD>>");
        System.out.println(
                String.format("NAME: %s\t\tDEPARTMENT: %s\nTEACHERS IN DEPARTMENT: \n", getName(), getDepartment(), getDepartmentTeachers()));
        for (Teacher teacher : getDepartmentTeachers()) {
            System.out.println("NAME: " + teacher.getName() + " SOCIAL SECURITY NUMBER: " + teacher.getSocialSecurityNumber() + " DEPARTMENT: "
                    + teacher.getDepartment() + " YEARS OF EXPERIENCE: "+ teacher.getYearsOfExperience() + " DATE OF BIRTH:" + teacher.getDateOfBirth());
        }
    }

    public void sort(EmployeeSortStrategy sortStrategy, String order) {
        sortStrategy.sort(departmentTeachers, order);
    }

    }