package entity;

import java.time.LocalDate;

public abstract class Employee {

    private String name;
    private Long socialSecurityNumber;
    private LocalDate dateOfBirth;
    private Department department;
    private int yearsOfExperience;


    public Employee() {
    }

    public Employee(String name, Long socialSecurityNumber, LocalDate dateOfBirth, Department department, int yearsOfExperience) {
        this.name = name;
        this.socialSecurityNumber = socialSecurityNumber;
        this.dateOfBirth = dateOfBirth;
        this.department = department;
        this.yearsOfExperience = yearsOfExperience;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(Long socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(int yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }


//    @Override
//    public String toString() {
//        return "Employee{" +
//                "name='" + name + '\'' +
//                ", socialSecurityNumber=" + socialSecurityNumber +
//                ", dateOfBirth=" + dateOfBirth +
//                ", department=" + department +
//                ", yearsOfExperience=" + yearsOfExperience +
//                '}';
//    }
}
