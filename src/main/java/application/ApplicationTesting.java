package application;

import entity.Department;
import entity.HeadOfDepartment;
import entity.Teacher;
import factory.EmployeeFactory;
import strategy.SortEmpByDateOfB;
import strategy.SortEmpByName;
import strategy.SortEmpByYear;
import java.time.LocalDate;
import static factory.BuildFactory.buildHOD;
import static factory.BuildFactory.buildTeacher;


public class ApplicationTesting {

    public static void main(String[] args) {
        EmployeeFactory employeeFactory = new EmployeeFactory();

        HeadOfDepartment head = buildHOD(employeeFactory, "Ada", 123456L, 6, Department.SCIENCE_MATH, LocalDate.of(1994, 3, 3));
        Teacher teacher = buildTeacher(employeeFactory, "Ahmed", 123456L, 10, Department.SCIENCE_MATH, LocalDate.of(1957, 2, 7), "BIOLOGY");
        Teacher teacher1 = buildTeacher(employeeFactory, "Max", 123456L, 20, Department.SCIENCE_MATH, LocalDate.of(1963, 2, 5), "LAW");
        Teacher teacher2 = buildTeacher(employeeFactory, "Zen", 123456L, 2, Department.SCIENCE_MATH, LocalDate.of(1996, 1, 31), "PSYCHOLOGY");

        if (head != null) {
            if (teacher != null) {
                head.addEmployee(teacher);
            }
            if (teacher1 != null) {
                head.addEmployee(teacher1);
            }
            if (teacher2 != null) {
                head.addEmployee(teacher2);
            }

            head.displayDetails();

            // sort teachers by years of experience
            System.out.println("SORT: Years of Experience");
            head.sort(new SortEmpByYear(), "asc");
            head.displayDetails();

            // sort teachers by years of experience
            System.out.println("SORT: Years of Experience");
            head.sort(new SortEmpByYear(), "desc");
            head.displayDetails();

            // sort teachers by name
            System.out.println("SORT: Name");
            head.sort(new SortEmpByName(), "desc");
            head.displayDetails();

            System.out.println("SORT: Name");
            head.sort(new SortEmpByName(), "asc");
            head.displayDetails();

            // sort teachers by dob
            System.out.println("SORT: Date of Birth");
            head.sort(new SortEmpByDateOfB(), "asc");
            head.displayDetails();
        }
    }

}
