package strategy;

import entity.Employee;

import java.util.Comparator;
import java.util.List;

public class SortEmpByDateOfB implements EmployeeSortStrategy{

        @Override
        public void sort(List<? extends Employee> employeeList, String order) {
            employeeList.sort(new Comparator<Employee>() {
                @Override
                public int compare(Employee o1, Employee o2) {
                    if (order == null || order.equalsIgnoreCase("asc") ||
                            order.equalsIgnoreCase("")) {
                        return o1.getDateOfBirth().compareTo(o2.getDateOfBirth());
                    }
                    return o2.getDateOfBirth().compareTo(o1.getDateOfBirth());
                }
            });
        }
    }
