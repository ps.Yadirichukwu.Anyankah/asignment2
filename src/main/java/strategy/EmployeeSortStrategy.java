package strategy;

import entity.Employee;

import java.util.List;

public interface EmployeeSortStrategy {

    void sort(List<? extends Employee> employeeList, String order);
}
