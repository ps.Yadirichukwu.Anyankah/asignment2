package strategy;

import entity.Employee;

import java.util.Comparator;
import java.util.List;

public class SortEmpByName implements EmployeeSortStrategy{

        @Override
        public void sort(List<? extends Employee> employees, String order) {
            employees.sort(new Comparator<Employee>() {
                @Override
                public int compare(Employee o1, Employee o2) {
                    String name1 = o1.getName();
                    String name2 = o2.getName();
                    if (order == null || order.equalsIgnoreCase("asc") ||
                            order.equalsIgnoreCase("")) {
                        return name2.compareTo(name1);
                    }
                    return name1.compareTo(name2);
                }
            });
        }
    }
