package strategy;

import entity.Employee;

import java.util.Comparator;
import java.util.List;

public class SortEmpByYear implements EmployeeSortStrategy{

        public void sort(List<? extends Employee> employees, String order) {
            employees.sort(new Comparator<Employee>() {
                @Override
                public int compare(Employee o1, Employee o2) {
                    Integer yoe1 = o1.getYearsOfExperience();
                    Integer yoe2 = o2.getYearsOfExperience();
                    if (order == null || order.equalsIgnoreCase("asc") ||
                            order.equalsIgnoreCase("")) {
                        return yoe1.compareTo(yoe2);
                    }
                    return yoe2.compareTo(yoe1);
                }
            });
        }
    }
